package com.lessons.task1;

public class Book implements Comparable<Book> {
    String name;
    String author;
    int publishingYear;

    public Book(String name, String author, int publishingYear) {
        this.name = name;
        this.author = author;
        this.publishingYear = publishingYear;
    }

    @Override
    public int compareTo(Book book) {
        return this.publishingYear - book.publishingYear;
    }

    @Override
    public String toString() {
        return "--> " +
                "Название: '" + name + '\'' +
                ", Автор: '" + author + '\'' +
                ", Год издания: " + publishingYear +
                ';';
    }
}
