package com.lessons.task1;

import java.util.Scanner;
import java.util.TreeSet;

public class MyLibrary {

    public static void main(String[] args) {
        TreeSet<Book> books = new TreeSet<>();
        books.add(new Book(
                "Глубокое обучение и TensorFlow для профессионалов",
                "Сантану Паттанаяк",
                2019));
        books.add(new Book(
                "Python для чайников",
                "Джон Пол Мюллер, Дебби Валковски",
                2020));
        books.add(new Book(
                "Рефакторинг кода на JavaScript: улучшение проекта существующего кода",
                "Мартин Фаулер",
                2020));
        books.add(new Book(
                "Java. Полное руководство, 10-е издание",
                "Герберт Шилдт",
                2018));
        books.add(new Book(
                "Java 8. Карманный справочник",
                "Роберт Лигуори, Патрисия Лигуори",
                2017));

        System.out.println("Самая старая книга: " + books.first().name + ". Год издания: " + books.first().publishingYear);
        Scanner in = new Scanner(System.in);

        lab: do {
            System.out.print("Введите автора, книгу которого нужно найти: ");
            String inString = in.nextLine();
            for (Book book : books) {
                if (book.author.equals(inString)){
                    System.out.println("\"" + book.name + "\"");
                    break lab;
                }
            }
            System.out.println("Имя автора введено не верно. Попробуй еще раз...");
        }while (true);

        System.out.print("\nВведите максимальный год издания для выводимых книг: ");
        int findPublishingYear = Integer.parseInt(in.nextLine());
        for (Book book : books) {
            if (book.publishingYear < findPublishingYear){
                System.out.println(book);
            }
        }

    }
}
