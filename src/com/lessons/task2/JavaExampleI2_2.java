package com.lessons.task2;

import java.util.Scanner;

public class JavaExampleI2_2 {

    /**
     * This program convert UAN to USD or EUR
     */
    public static void main(String[] args) {

        String USD = "USD";
        String EUR = "EUR";
        String RUB = "RUB";

        Bank[] banks = {new Bank("ПриватБанк", 28.5f, 31.75f, 0.365f),
                new Bank("Райффайзен Банк Аваль", 28.45f, 31.5f, 0.367f),
                new Bank("Альфа-Банк", 28.60f, 31.3f, 0.38f)};


        //init scanner
        Scanner scan = new Scanner(System.in);


        Bank userBank = null;
        while (true) {
            System.out.print("Enter the bank: ");
            String userBankName = scan.nextLine();
            //try to find bank
            for (Bank bank : banks) {
                if (bank.name.equalsIgnoreCase(userBankName)) {
                    userBank = bank;
                }
            }
            //check the result of comparison
            if (userBank != null) {
                break;
            } else {
                System.out.println("Can't find the bank. Tray Again...");
            }
        }

        //enter amount of money
        System.out.print("Enter the amount of money that you want to change in UAH: ");
        int amount = Integer.parseInt(scan.nextLine());


        //enter currency
        System.out.print("Enter the currency to convert (USD, EUR or RUB): ");
        String currency = scan.nextLine();

        if (USD.equalsIgnoreCase(currency)) {
            System.out.println(String.format("You money in %s: %.2f", USD, amount / userBank.courseUSD));
        } else if (EUR.equalsIgnoreCase(currency)) {
            System.out.println(String.format("You money in %s: %.2f", EUR, amount / userBank.courseEUR));
        } else if (RUB.equalsIgnoreCase(currency)) {
            System.out.println(String.format("You money in %s: %d", RUB, Math.round(amount / userBank.courseRUB)));
        } else {
            System.err.println("Can't convert to " + currency);
        }

    }

}
