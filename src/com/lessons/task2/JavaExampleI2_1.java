package com.lessons.task2;

import java.util.Scanner;

public class JavaExampleI2_1 {

    /**
     * This program convert UAN to USD or EUR
     */
    public static void main(String[] args) {

        String USD = "USD";
        float course;

        Bank[] banks = {new Bank("ПриватБанк", 28.5f),
                new Bank("Райффайзен Банк Аваль", 28.45f),
                new Bank("Альфа-Банк", 28.60f)};


        //init scanner
        Scanner scan = new Scanner(System.in);

        lab: do {
            System.out.print("Enter the bank: ");
            String userBank = scan.nextLine();
            for (Bank bank : banks) {
                if (bank.name.equalsIgnoreCase(userBank)) {
                    course = bank.courseUSD;
                    break lab;
                }
            }
            System.out.println("Can't find the bank. Tray Again...");
        } while (true);

        //enter amount of money
        System.out.print("Enter the amount of money that you want to change in UAH: ");
        int amount = Integer.parseInt(scan.nextLine());

        System.out.println(String.format("You money in %s: %.2f", USD, amount / course));

    }


}