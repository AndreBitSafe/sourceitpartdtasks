package com.lessons.task2;

public class Bank {
     String name;
     float courseUSD;
     float courseEUR;
     float courseRUB;

    public Bank(String name, float courseUSD) {
        this.name = name;
        this.courseUSD = courseUSD;
    }

    public Bank(String name, float courseUSD, float courseEUR, float courseRUB) {
        this.name = name;
        this.courseUSD = courseUSD;
        this.courseEUR = courseEUR;
        this.courseRUB = courseRUB;
    }
}
